import App from './app';
import { CrudController } from './controller/crud_controller';
import { InMemoryRepo } from './repo/in_memory_repo';
import { ICustomer, Customer } from './domain/customer';
import { IExecutor, Executor } from './domain/executor';
import { IProject, Project } from './domain/project';
import { IExecution, Execution } from './domain/execution';


const application = new App(
    Number(process.env.PORT).valueOf() || 3000,
    [
        new CrudController('/customers', new InMemoryRepo<ICustomer>([
            Customer('Andrii', 20_000),
            Customer('NU LP', 999_999),
            Customer('LNMU medical university', 1_550_000)
        ])),
        new CrudController('/executors', new InMemoryRepo<IExecutor>([
            Executor('SoftServe Inc.', 20, 1300),
            Executor('EPAM', 25, 3000),
            Executor('JetSoftPro', 5, 100)
        ])),
        new CrudController('/projects', new InMemoryRepo<IProject>([
            Project('Healthcare platform', 2),
            Project('Virtual educational center', 1, 'A scalable platform for MOOCs')
        ])),
        new CrudController('/executions', new InMemoryRepo<IExecution>([
            Execution(0, 1, new Date('2014-01-04'), new Date('2020-01-01')),
            Execution(1, 0, new Date('2018-05-11'))
        ]))
    ]
);

application.serveForever();