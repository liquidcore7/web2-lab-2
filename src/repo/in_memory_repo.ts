import { Entity, IWithID } from '../domain/entity';
import * as _ from 'lodash';
import { ICrudRepo } from './crud_repo';

export class InMemoryRepo<I extends IWithID> implements ICrudRepo<I> {
  private readonly elements: Array<Entity<I> | null>;
  private readonly droppedIds: number[] = [];

  constructor(of?: I[]) {
    this.elements = _.map(
      { ...of } || {},
      (value: I, key: string) => new Entity<I>(value).withId(Number(key).valueOf())
    );
  }

  findById(id: number): Entity<I> | null {
    return this.elements.length > id ? this.elements[id] : null;
  }

  create(elem: I): Entity<I> {
    const generatedId: number = this.droppedIds.pop() || this.elements.length;
    const entity = new Entity<I>(elem);
    const entityWithId = entity.withId(generatedId);

    if (generatedId < this.elements.length) {
      this.elements[generatedId] = entityWithId;
    } else this.elements.push(entityWithId);

    return entityWithId;
  }

  deleteById(id: number): void {
    this.droppedIds.push(id);
    this.elements[id] = null;
  }

  update(oldId: number, newElem: I): Entity<I> | null {
    const oldElem: Entity<I> | null = this.findById(oldId);
    if (oldElem != null) {
      const newEntity = new Entity<I>(newElem);
      const withCorrectId = newEntity.withId(oldId);
      this.elements[oldId] = withCorrectId;
      return withCorrectId;
    }
    return oldElem;
  }

  fetchAll(): Entity<I>[] {
    return this.elements
      .filter(entity => entity != null)
      .map(entity => entity as Entity<I>);
  }
}
