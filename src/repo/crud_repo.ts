import { Entity, IWithID } from '../domain/entity';


export interface ICrudRepo<I extends IWithID> {
  findById(id: number): Entity<I> | null;

  create(elem: I): Entity<I>;

  deleteById(id: number): void;

  update(oldId: number, newElem: I): Entity<I> | null;

  fetchAll(): Entity<I>[];
}
