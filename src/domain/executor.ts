import { IWithID } from "./entity";


export interface IExecutor extends IWithID {
    readonly name: string;
    readonly experience: number;
    readonly employees: number;
};

export const Executor = (name: string, experience: number, employees: number): IExecutor => ({
    name: name,
    experience: experience,
    employees: employees
});