import { Entity, IWithID } from './entity';


export interface IProject extends IWithID {
  readonly name: string;
  readonly description?: string;
  readonly customerId: number;
};

export const Project = (name: string, customerId: number, description?: string): IProject => ({
  name: name,
  description: description,
  customerId: customerId
});
