import { IWithID } from "./entity";


export interface IExecution extends IWithID {
    readonly projectId: number;
    readonly executorId: number;
    readonly startDate: Date;
    readonly endDate: Date;
};

export const Execution = (projectId: number, executorId: number, startDate?: Date, endDate?: Date): IExecution => ({
    projectId: projectId,
    executorId: executorId,
    startDate: startDate == undefined ? new Date() : startDate as Date,
    endDate: endDate == undefined ? new Date() : endDate as Date
});