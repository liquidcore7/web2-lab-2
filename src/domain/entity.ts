export interface IWithID {
  id?: number;
};

export class Entity<T extends IWithID> {
  public readonly body: T;

  constructor(body: T) {
    this.body = body;
  }

  withId(newId: number): Entity<T> {
    this.body.id = newId;
    return this;
  }
};