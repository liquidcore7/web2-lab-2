import { ICrudRepo } from "../repo/crud_repo";
import { Entity, IWithID } from "../domain/entity";
import { Request, Response, Application } from "express";
import { ParamsDictionary } from 'express-serve-static-core';


export class CrudController<I extends IWithID> {
    private readonly route: string;
    private readonly pugRoute: string;

    private readonly crudRepo: ICrudRepo<I>;

    constructor(
        route: string, 
        crudRepo: ICrudRepo<I>,
        pugRoute?: string) {
            this.route = route;
            this.pugRoute = pugRoute || ('entities' + route);
            this.crudRepo = crudRepo; 
    }

    private redirectToList = (req: Request, res: Response): void => {
        res.redirect(req.baseUrl + this.route + '/list');
    }


    getCreateForm = (req: Request, res: Response): void => {
        res.render(this.pugRoute + '/create');
    }

    getUpdateForm = (req: Request<IIdParams>, res: Response): void => {
        const entity = this.crudRepo.findById(
            Number(req.params.id).valueOf()
        ) as Entity<I>;
        res.render(this.pugRoute + '/update', new EntityResponse(entity));
    }

    create = (req: Request, res: Response): void => {
        this.crudRepo.create( req.body as I );
        this.redirectToList(req, res);
    }


    list = (req: Request, res: Response): void => {
        res.render(this.pugRoute + '/list', new ListResponse(this.crudRepo.fetchAll()));
    }

    delete = (req: Request<IIdParams>, res: Response): void => {
        this.crudRepo.deleteById(
            Number(req.params.id).valueOf()
        );
        this.redirectToList(req, res);
    }

    update = (req: Request<IIdParams>, res: Response): void => {
        const id = Number(req.params.id).valueOf();
        this.crudRepo.update(id, req.body as I);

        this.redirectToList(req, res);
    }


    bindToApp = (app: Application): Application =>
        app.get(this.route + '/list', this.list)
            .get(this.route + '/create', this.getCreateForm)
            .get(this.route + '/update/:id', this.getUpdateForm)
            .post(this.route, this.create)
            .post(this.route + '/:id', this.update)
            .get(this.route + '/delete/:id', this.delete);
};


interface IIdParams extends ParamsDictionary {
    id: string; 
};

class ListResponse<I extends IWithID> {
    public readonly entities: I[];

    constructor(entities: Entity<I>[]) {
        this.entities = entities.map(e => e.body);
    }
};

class EntityResponse<I extends IWithID> {
    public readonly entity: I;

    constructor(entity: Entity<I>) {
        this.entity = entity.body;
    }
}
