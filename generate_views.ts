import { Project, SourceFile, InterfaceDeclaration, SyntaxKind, PropertySignature } from 'ts-morph';
import * as fs from 'fs';
import { startCase } from 'lodash';


interface TemplateGenerator {
    readonly filename: string;
    readonly content: (name: string, chNodes: PropertySignature[]) => string;
}

const PugGenerators: TemplateGenerator[] = [
    { filename: 'create.pug',
      content: (name: string, chNodes: PropertySignature[]): string => {

        const formGroups: string = chNodes.map(prop => {
            const pName = prop.getName();
            const readableName = startCase(pName);
            const inpName = pName + 'Input';
            
            return `    .form-group
      label(for='${inpName}') ${readableName}
      input#${inpName}.form-control(type='text' name='${pName}')`;
        }).reduce((l, r) => `${l}\n${r}`);

        return `extends ../../index.pug
include ../../fielddef.pug

block submitForm
  form.mt-3(action='/${name}', method='post')
${formGroups}
    .btn-group(role='group' aria-label='Cancel or submit')
      button.btn.btn-secondary(type='button', onclick="window.location = '/customers/list';") Cancel  
      button.btn.btn-primary(type='submit') Create
`;
    }}, 
    {
      filename: 'list.pug',
      content: (name: string, chProps: PropertySignature[]): string => {
        const href = (action: 'update' | 'delete'): string => "`/" + name + "/" + action + "/${entity.id}`";
        const createHref = "`/" + name + "/create`";
        const tdNodesAsStr: string = chProps
            .map(node => `    td #{entity.${node.getName()}}`)
            .reduce((l, r) => `${l}\n${r}`);

        const thNamesAsStr: string = chProps
            .map(node => `        th(scope='col') ${startCase(node.getName())}`)
            .reduce((l, r) => `${l}\n${r}`);

        return `extends ../../index.pug

mixin entityTr(entity)
  tr
    th(scope='row') #{entity.id}
${tdNodesAsStr}
    td 
      a.btn-sm.btn-secondary(href=${href('update')}) Update
    td
      a.btn-sm.btn-danger(href=${href('delete')}) Delete

block submitForm
  a.mt-5.btn.btn-primary(href=${createHref}) Add new

block entityTable
  table.table
    thead
      tr
        th(scope='col') ID
${thNamesAsStr}
        th(scope='col') 
        th(scope='col') 
    tbody
      for entity in entities
        +entityTr(entity)`;
    }},
    {
      filename: 'update.pug',
      content: (name: string, chProps: PropertySignature[]): string => {
        const action = "`/" + name + "/${entity.id}`";
        const formGroups: string = chProps.map(prop => {
            const pName = prop.getName();
            const readableName = startCase(pName);
            const inpName = pName + 'Input';
            const placeholderText = "`${entity." + pName + "}`";
            
            return `    .form-group
      label(for='${inpName}') ${readableName}
      input#${inpName}.form-control(type='text' name='${pName}' value=${placeholderText})`;
        }).reduce((l, r) => `${l}\n${r}`);
        const entityIdPlaceholder = "`${entity.id}`";

        return `extends ../../index.pug
include ../../fielddef.pug
                
block submitForm
  form.mt-3(action=${action}, method='post')
    .form-group
      label(for='idInput') ID
      input#idInput.form-control(type='text' name='id' placeholder=${entityIdPlaceholder} readonly)
${formGroups}    
    .btn-group(role='group' aria-label='Cancel or submit')
      button.btn.btn-secondary(type='button', onclick="window.location = '/${name}/list';") Cancel  
      button.btn.btn-primary(type='submit') Update`;
    }}
];

console.info('Instatiating compiler...');

const project = new Project({
    // tsConfigFilePath: './tsconfig.json',
    addFilesFromTsConfig: false
});

const childInterfacePaths = project.addSourceFilesAtPaths('src/domain/*.ts');
console.info('Added project files.');

const entityBaseFile: SourceFile = project.getSourceFileOrThrow('src/domain/entity.ts');

const iWithId: InterfaceDeclaration = entityBaseFile.getInterfaceOrThrow('IWithID');
console.info('Main interface file loaded.');

const iEntities: InterfaceDeclaration[] = childInterfacePaths
  .map(path => path.getStatementByKind(SyntaxKind.InterfaceDeclaration))
  .filter(iface => iface != undefined)
  .map(iface => iface as InterfaceDeclaration)
  .filter(iface => {
    const parents = iface.getExtends().map(e => e.getText());
    return parents.indexOf(iWithId.getName()) >= 0;
  });


console.info(`Found ${iEntities.length} descendants.`);

iEntities.forEach(childInterface => {
    console.info(`Creating views for ${childInterface.getName()} ...`);

    const name = childInterface
        .getName()
        .substr(1)  // drop 'I' prefix
        .toLowerCase() + 's';

    const nodes: PropertySignature[] = childInterface.getProperties();

    PugGenerators.forEach(generator => {
        console.info(`Writing ${name}/${generator.filename}...`);

        const viewsPath = `./static/views/entities/${name}/`;

        fs.mkdirSync(viewsPath, { recursive: true });
        fs.writeFileSync(
            viewsPath + generator.filename,
            generator.content(name, nodes),
            'utf-8'
        );
    });
});
